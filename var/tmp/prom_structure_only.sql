/*
 Source Server         : prom
 Source Server Type    : SQLite
 File Encoding         : utf-8

 Date: 03/07/2016 12:15:57 PM
*/

PRAGMA foreign_keys = false;

-- ----------------------------
--  Table structure for alembic_version
-- ----------------------------
DROP TABLE IF EXISTS "alembic_version";
CREATE TABLE alembic_version (
	version_num VARCHAR(32) NOT NULL
);

-- ----------------------------
--  Table structure for auth_roles
-- ----------------------------
DROP TABLE IF EXISTS "auth_roles";
CREATE TABLE auth_roles (
	id INTEGER NOT NULL, 
	name VARCHAR(80) NOT NULL, 
	user_id INTEGER, 
	PRIMARY KEY (id), 
	FOREIGN KEY(user_id) REFERENCES auth_users (id), 
	UNIQUE (name)
);

-- ----------------------------
--  Table structure for auth_users
-- ----------------------------
DROP TABLE IF EXISTS "auth_users";
CREATE TABLE auth_users (
	id INTEGER NOT NULL, 
	username VARCHAR(80) NOT NULL, 
	email VARCHAR(80) NOT NULL, 
	password VARCHAR(128), 
	created_at DATETIME NOT NULL, 
	first_name VARCHAR(30), 
	last_name VARCHAR(30), 
	PRIMARY KEY (id), 
	UNIQUE (email), 
	UNIQUE (username)
);

-- ----------------------------
--  Table structure for library_author
-- ----------------------------
DROP TABLE IF EXISTS "library_author";
CREATE TABLE library_author (
	id INTEGER NOT NULL, 
	"Name" VARCHAR(50) NOT NULL, 
	PRIMARY KEY (id)
);

-- ----------------------------
--  Table structure for library_author_books
-- ----------------------------
DROP TABLE IF EXISTS "library_author_books";
CREATE TABLE library_author_books (
	book_id INTEGER, 
	author_id INTEGER, 
	FOREIGN KEY(author_id) REFERENCES library_author (id), 
	FOREIGN KEY(book_id) REFERENCES library_book (id)
);

-- ----------------------------
--  Table structure for library_book
-- ----------------------------
DROP TABLE IF EXISTS "library_book";
CREATE TABLE library_book (
	id INTEGER NOT NULL, 
	"Title" VARCHAR(80) NOT NULL, 
	PRIMARY KEY (id)
);

PRAGMA foreign_keys = true;
