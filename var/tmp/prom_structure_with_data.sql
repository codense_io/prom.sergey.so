/*
 Source Server         : prom
 Source Server Type    : SQLite
 File Encoding         : utf-8

 Date: 03/07/2016 12:15:57 PM
*/

PRAGMA foreign_keys = false;

-- ----------------------------
--  Table structure for alembic_version
-- ----------------------------
DROP TABLE IF EXISTS "alembic_version";
CREATE TABLE alembic_version (
	version_num VARCHAR(32) NOT NULL
);

-- ----------------------------
--  Records of alembic_version
-- ----------------------------
BEGIN;
INSERT INTO "alembic_version" VALUES ('ce724c1354f0');
COMMIT;

-- ----------------------------
--  Table structure for auth_roles
-- ----------------------------
DROP TABLE IF EXISTS "auth_roles";
CREATE TABLE auth_roles (
	id INTEGER NOT NULL, 
	name VARCHAR(80) NOT NULL, 
	user_id INTEGER, 
	PRIMARY KEY (id), 
	FOREIGN KEY(user_id) REFERENCES auth_users (id), 
	UNIQUE (name)
);

-- ----------------------------
--  Table structure for auth_users
-- ----------------------------
DROP TABLE IF EXISTS "auth_users";
CREATE TABLE auth_users (
	id INTEGER NOT NULL, 
	username VARCHAR(80) NOT NULL, 
	email VARCHAR(80) NOT NULL, 
	password VARCHAR(128), 
	created_at DATETIME NOT NULL, 
	first_name VARCHAR(30), 
	last_name VARCHAR(30), 
	PRIMARY KEY (id), 
	UNIQUE (email), 
	UNIQUE (username)
);

-- ----------------------------
--  Table structure for library_author
-- ----------------------------
DROP TABLE IF EXISTS "library_author";
CREATE TABLE library_author (
	id INTEGER NOT NULL, 
	"Name" VARCHAR(50) NOT NULL, 
	PRIMARY KEY (id)
);

-- ----------------------------
--  Records of library_author
-- ----------------------------
BEGIN;
INSERT INTO "library_author" VALUES (1, 'David M. Beazley');
INSERT INTO "library_author" VALUES (2, 'John M. Zelle');
INSERT INTO "library_author" VALUES (3, 'Прохоренок Николай Анатольевич');
INSERT INTO "library_author" VALUES (4, 'Paul Ferrill');
INSERT INTO "library_author" VALUES (5, 'Steven Bird');
INSERT INTO "library_author" VALUES (6, 'Ewan Klein');
INSERT INTO "library_author" VALUES (7, 'Edward Loper');
INSERT INTO "library_author" VALUES (8, 'David Mertz');
INSERT INTO "library_author" VALUES (9, 'Alex Martelli');
INSERT INTO "library_author" VALUES (10, 'Anna Ravenscroft');
INSERT INTO "library_author" VALUES (11, 'David Ascher');
INSERT INTO "library_author" VALUES (12, 'Wesley Chun');
INSERT INTO "library_author" VALUES (13, 'Joseph Eddy Fontenrose');
INSERT INTO "library_author" VALUES (14, 'Mark Lutz');
INSERT INTO "library_author" VALUES (15, 'Chris Fehily');
INSERT INTO "library_author" VALUES (16, 'Jentezen Franklin');
INSERT INTO "library_author" VALUES (17, 'Rytis Sileika');
INSERT INTO "library_author" VALUES (18, 'Justin Seitz');
INSERT INTO "library_author" VALUES (19, 'Matthew A. Telles');
INSERT INTO "library_author" VALUES (20, 'Kenneth Lambert');
INSERT INTO "library_author" VALUES (21, 'Brandon Rhodes');
INSERT INTO "library_author" VALUES (22, 'John Goerzen');
INSERT INTO "library_author" VALUES (23, 'Jason Kinser');
INSERT INTO "library_author" VALUES (24, 'Mark Summerfield');
INSERT INTO "library_author" VALUES (25, 'Jeff Knupp');
INSERT INTO "library_author" VALUES (26, 'Jan Solem');
INSERT INTO "library_author" VALUES (27, 'Wes McKinney');
INSERT INTO "library_author" VALUES (28, 'Magnus Lie Hetland');
INSERT INTO "library_author" VALUES (29, 'Майк МакГрат');
INSERT INTO "library_author" VALUES (30, 'Дронов Владимир Александрович');
INSERT INTO "library_author" VALUES (31, 'Sarah Mount');
INSERT INTO "library_author" VALUES (32, 'James Shuttleworth');
INSERT INTO "library_author" VALUES (33, 'Russel Winder');
INSERT INTO "library_author" VALUES (34, 'Hans Petter Langtangen');
INSERT INTO "library_author" VALUES (35, 'Niall O''Higgins');
INSERT INTO "library_author" VALUES (36, 'Сузи Роман Арвиевич');
INSERT INTO "library_author" VALUES (37, 'Paul Barry');
INSERT INTO "library_author" VALUES (38, 'Marty Alchin');
INSERT INTO "library_author" VALUES (39, 'J. Burton Browning');
INSERT INTO "library_author" VALUES (40, 'Steve Holden');
INSERT INTO "library_author" VALUES (41, 'Dusty Phillips');
INSERT INTO "library_author" VALUES (42, 'Mitch Garnaat');
INSERT INTO "library_author" VALUES (43, 'TJ O''Connor');
COMMIT;

-- ----------------------------
--  Table structure for library_author_books
-- ----------------------------
DROP TABLE IF EXISTS "library_author_books";
CREATE TABLE library_author_books (
	book_id INTEGER, 
	author_id INTEGER, 
	FOREIGN KEY(author_id) REFERENCES library_author (id), 
	FOREIGN KEY(book_id) REFERENCES library_book (id)
);

-- ----------------------------
--  Records of library_author_books
-- ----------------------------
BEGIN;
INSERT INTO "library_author_books" VALUES (1, 1);
INSERT INTO "library_author_books" VALUES (2, 2);
INSERT INTO "library_author_books" VALUES (3, 3);
INSERT INTO "library_author_books" VALUES (4, 4);
INSERT INTO "library_author_books" VALUES (5, 5);
INSERT INTO "library_author_books" VALUES (5, 6);
INSERT INTO "library_author_books" VALUES (5, 7);
INSERT INTO "library_author_books" VALUES (6, 8);
INSERT INTO "library_author_books" VALUES (7, 9);
INSERT INTO "library_author_books" VALUES (9, 10);
INSERT INTO "library_author_books" VALUES (9, 11);
INSERT INTO "library_author_books" VALUES (10, 12);
INSERT INTO "library_author_books" VALUES (11, 13);
INSERT INTO "library_author_books" VALUES (12, 14);
INSERT INTO "library_author_books" VALUES (13, 15);
INSERT INTO "library_author_books" VALUES (14, 16);
INSERT INTO "library_author_books" VALUES (15, 17);
INSERT INTO "library_author_books" VALUES (16, 18);
INSERT INTO "library_author_books" VALUES (17, 19);
INSERT INTO "library_author_books" VALUES (19, 20);
INSERT INTO "library_author_books" VALUES (20, 21);
INSERT INTO "library_author_books" VALUES (20, 22);
INSERT INTO "library_author_books" VALUES (21, 23);
INSERT INTO "library_author_books" VALUES (22, 24);
INSERT INTO "library_author_books" VALUES (23, 25);
INSERT INTO "library_author_books" VALUES (24, 26);
INSERT INTO "library_author_books" VALUES (25, 27);
INSERT INTO "library_author_books" VALUES (26, 28);
INSERT INTO "library_author_books" VALUES (27, 29);
INSERT INTO "library_author_books" VALUES (28, 30);
INSERT INTO "library_author_books" VALUES (29, 31);
INSERT INTO "library_author_books" VALUES (29, 33);
INSERT INTO "library_author_books" VALUES (29, 32);
INSERT INTO "library_author_books" VALUES (30, 34);
INSERT INTO "library_author_books" VALUES (32, 35);
INSERT INTO "library_author_books" VALUES (33, 36);
INSERT INTO "library_author_books" VALUES (34, 37);
INSERT INTO "library_author_books" VALUES (35, 39);
INSERT INTO "library_author_books" VALUES (35, 38);
INSERT INTO "library_author_books" VALUES (36, 40);
INSERT INTO "library_author_books" VALUES (37, 41);
INSERT INTO "library_author_books" VALUES (38, 42);
INSERT INTO "library_author_books" VALUES (40, 43);
COMMIT;

-- ----------------------------
--  Table structure for library_book
-- ----------------------------
DROP TABLE IF EXISTS "library_book";
CREATE TABLE library_book (
	id INTEGER NOT NULL, 
	"Title" VARCHAR(80) NOT NULL, 
	PRIMARY KEY (id)
);

-- ----------------------------
--  Records of library_book
-- ----------------------------
BEGIN;
INSERT INTO "library_book" VALUES (1, 'Python Essential Reference');
INSERT INTO "library_book" VALUES (2, 'Python Programming');
INSERT INTO "library_book" VALUES (3, 'Python');
INSERT INTO "library_book" VALUES (4, 'Pro Android Python with SL4A');
INSERT INTO "library_book" VALUES (5, 'Natural Language Processing with Python');
INSERT INTO "library_book" VALUES (6, 'Text Processing in Python');
INSERT INTO "library_book" VALUES (7, 'Python in a Nutshell');
INSERT INTO "library_book" VALUES (8, 'Python 3 и PyQt. Разработка приложений');
INSERT INTO "library_book" VALUES (9, 'Python Cookbook');
INSERT INTO "library_book" VALUES (10, 'Core Python Programming');
INSERT INTO "library_book" VALUES (11, 'Python');
INSERT INTO "library_book" VALUES (12, 'Learning Python');
INSERT INTO "library_book" VALUES (13, 'Python');
INSERT INTO "library_book" VALUES (14, 'The Spirit of Python');
INSERT INTO "library_book" VALUES (15, 'Pro Python System Administration');
INSERT INTO "library_book" VALUES (16, 'Gray Hat Python');
INSERT INTO "library_book" VALUES (17, 'Python Power!');
INSERT INTO "library_book" VALUES (18, 'Programming Python');
INSERT INTO "library_book" VALUES (19, 'Fundamentals of Python: From First Programs through Data Structures');
INSERT INTO "library_book" VALUES (20, 'Foundations of Python Network Programming');
INSERT INTO "library_book" VALUES (21, 'Python for Bioinformatics');
INSERT INTO "library_book" VALUES (22, 'Advanced Python 3 Programming Techniques');
INSERT INTO "library_book" VALUES (23, 'Writing Idiomatic Python 2.7.3');
INSERT INTO "library_book" VALUES (24, 'Programming Computer Vision with Python');
INSERT INTO "library_book" VALUES (25, 'Python for Data Analysis');
INSERT INTO "library_book" VALUES (26, 'Python Algorithms');
INSERT INTO "library_book" VALUES (27, 'Python. Программирование для начинающих');
INSERT INTO "library_book" VALUES (28, 'Django: практика создания Web-сайтов на Python');
INSERT INTO "library_book" VALUES (29, 'Python for Rookies');
INSERT INTO "library_book" VALUES (30, 'A Primer on Scientific Programming with Python');
INSERT INTO "library_book" VALUES (31, 'Python Scripting for Computational Science');
INSERT INTO "library_book" VALUES (32, 'MongoDB and Python');
INSERT INTO "library_book" VALUES (33, 'Python (+CD-ROM)');
INSERT INTO "library_book" VALUES (34, 'Head First Python');
INSERT INTO "library_book" VALUES (35, 'Pro Python');
INSERT INTO "library_book" VALUES (36, 'Python Web Programming');
INSERT INTO "library_book" VALUES (37, 'Python 3 Object Oriented Programming');
INSERT INTO "library_book" VALUES (38, 'Python and AWS Cookbook');
INSERT INTO "library_book" VALUES (39, 'Fundamentals of Python: First Programs');
INSERT INTO "library_book" VALUES (40, 'Violent Python');
COMMIT;

PRAGMA foreign_keys = true;
