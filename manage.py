#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from flask_migrate import Migrate, MigrateCommand
from flask_script import Command, Manager, Option, Server, Shell
from flask_script.commands import Clean, ShowUrls

from prom.app import create_app
from prom.database import db
from prom.user.models import User
from prom.library.models import Book, Author
from prom.commands import LoadTestData, CleanTestData

if os.environ.get('PROM_ENV') == 'staging':
    from prom.config.staging import StagingConfig
    CONFIG = StagingConfig
else:
    from prom.config.production import ProductionConfig
    CONFIG = ProductionConfig

app = create_app(CONFIG)
manager = Manager(app)
migrate = Migrate(app, db)

def _make_context():
    return {'app': app, 'db': db, 'User': User, 'Author': Author, 'Book': Book}

manager.add_command('server', Server())
manager.add_command('shell', Shell(make_context=_make_context))
manager.add_command('db', MigrateCommand)
manager.add_command('urls', ShowUrls())
manager.add_command('clean', Clean())
manager.add_command('loadtestdata', LoadTestData())
manager.add_command('cleantestdata', CleanTestData())

if __name__ == '__main__':
    manager.run()
