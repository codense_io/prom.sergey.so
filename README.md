# Prom Library

Yet another Flask app

## Quickstart

---

### Environment variables

First, set your app's secret key as an environment variable.
For example, example add the following to `.bashrc` or `.bash_profile` or virtualenv `activate` script

```
export PROM_ENV='staging'
export PROM_SECRET='awesome-mega-strong-secret-key'
```

### Install
```
$ git clone https://codense_io@bitbucket.org/codense_io/prom.sergey.so.git
$ cd prom.sergey.so
```

#### Install the dependent packages
`$ pip install -r requirements/staging.txt`

#### Initial SQLite database
- `$ python manage.py db init`
- `$ python manage.py db migrate`
- `$ python manage.py db upgrate`

#### If you prefer work with test data, run:

Create test data:
`$ python manage.py loadtestdata`

Remove test data
`$ python manage.py cleantestdata`

After this will be nice to restart your gunicorn daemon or run dev webserver:
`$ python manage.py server`

Also you can view schema or execute it with SQL-queries (SQLite). Check:

- `var/tmp/prom_structure_only.sql`
- `var/tmp/prom_structure_with_data.sql`

Check `var` folder to view nginx/gunicorn sample configs and other deployment stuff.

---

## Demo site:

- Demo: http://prom.sergey.so/
- user: demo
- pass: welcome

---

## Contacts:

- http://sergey.so/
- mail: serj.soroka@gmail.com
- phone: +380675252552
