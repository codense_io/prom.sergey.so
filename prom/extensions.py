# -*- coding: utf-8 -*-
"""Extensions module : app factory located in app.py."""
from flask_bcrypt import Bcrypt
from flask_cache import Cache
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_paginate import Pagination
from flask_debugtoolbar import DebugToolbarExtension

bcrypt = Bcrypt()
login_manager = LoginManager()
db = SQLAlchemy()
migrate = Migrate()
cache = Cache()
api = Api()
pagination = Pagination()
debug_toolbar = DebugToolbarExtension()
