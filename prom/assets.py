# -*- coding: utf-8 -*-
"""Application assets."""
from flask_assets import Bundle, Environment

css = Bundle(
    '_assets/sass/common.scss',
    filters=['pyscss', 'cssmin'],
    output='css/common.min.css'
)

js = Bundle(
    '_assets/libs/jQuery/dist/jquery.js',
    '_assets/libs/bootstrap/dist/js/bootstrap.js',
    '_assets/libs/bootbox/bootbox.min.js',
    '_assets/libs/selectize/dist/js/standalone/selectize.js',
    '_assets/js/common.js',
    filters='jsmin',
    output='js/common.min.js'
)

assets = Environment()

assets.register('js_all', js)
assets.register('css_all', css)
