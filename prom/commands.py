# -*- coding: utf-8 -*-

import urllib2
import json

from flask.ext.script import Command
from prom.database import session
from prom.library.models import Book, Author


class LoadTestData(Command):
    "Load test data from Google Books API"

    def run(self):
        url = ('https://www.googleapis.com/books/v1/volumes',
               '?q=python&maxResults=40')

        response = urllib2.urlopen(url)
        data = json.load(response)
        for item in data['items']:
            try:
                title = item['volumeInfo']['title']
                authors = item['volumeInfo']['authors']

                book = Book(title)

                for name in authors:
                    if not session.query(Author).filter_by(name=name).first():
                        author = Author(name)
                        book.authors.append(author)

                    print u"{title}\t{author}".format(title=title, author=name)
                session.add(book)
                session.add(author)
                session.commit()
            except:
                session.rollback()


class CleanTestData(Command):
    """
    Clean test data from Google Books API

    Warning: delete all records!
    """

    def run(self):
        try:
            Book.query.delete()
            Author.query.delete()
            session.commit()
        except:
            session.rollback()
