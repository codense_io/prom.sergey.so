# -*- coding: utf-8 -*-

import os
from prom.app import create_app

if os.environ.get('PROM_ENV') == 'staging':
    from prom.config.staging import StagingConfig
    CONFIG = StagingConfig
else:
    from prom.config.production import ProductionConfig
    CONFIG = ProductionConfig

application = create_app(CONFIG)

if __name__ == "__main__":
    application.run()
