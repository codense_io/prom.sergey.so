# -*- coding: utf-8 -*-

import os
from unipath import Path


class Config(object):
    """Base configuration."""
    SECRET_KEY = os.environ.get('PROM_SECRET', 'secret-key')
    PROJECT_ROOT = Path.cwd()
    APP_DIR = Path(PROJECT_ROOT, 'prom')

    BCRYPT_LOG_ROUNDS = 13
    ASSETS_DEBUG = False
    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CACHE_TYPE = 'simple'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
