# -*- coding: utf-8 -*-

from .base import Config


class ProductionConfig(Config):
    ENV = 'production'
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/library'  # Chanhe it
    DEBUG_TB_ENABLED = False
