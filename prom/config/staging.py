# -*- coding: utf-8 -*-

from unipath import Path
from .base import Config


class StagingConfig(Config):
    """Staging configuration."""
    ENV = 'staging'
    DEBUG = False
    DB_NAME = 'prom.db'
    DB_PATH = Path(Config.PROJECT_ROOT, DB_NAME)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{0}'.format(DB_PATH)
    DEBUG_TB_ENABLED = False
    ASSETS_DEBUG = False
    MINIFY_PAGE = True
    CACHE_TYPE = 'simple'
