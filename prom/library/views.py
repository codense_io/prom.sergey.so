# -*- coding: utf-8 -*-
"""Library section, including homepage and signup."""

from flask import Blueprint, redirect, render_template, request, url_for
from htmlmin.minify import html_minify

from prom.database import session
from prom.utils import register_crud, flash_errors
from prom.views import ModelView
from prom.user.forms import LoginForm
from prom.library.models import Book, Author, author_books
from prom.library.forms import SearchForm


blueprint = Blueprint('library', __name__, static_folder='../static')


@blueprint.after_request
def response_minify(response):
    """minify html response to decrease site traffic"""

    if response.content_type == u'text/html; charset=utf-8':
        response.set_data(html_minify(response.get_data(as_text=True)))
        return response
    return response


@blueprint.route('/')
def home():
    """Home page"""

    newest_books = session.query(Book).order_by(Book.id.desc()).limit(4)
    return render_template('library/home.html', newest_books=newest_books)


class BookView(ModelView):
    """Class-based Book View"""

    model = Book
    list_template = 'library/book_listview.html'
    detail_template = 'library/book_detailview.html'
    per_page = 20
register_crud(blueprint, '/books', 'books', BookView)


class AuthorView(ModelView):
    """Class-based Author View"""

    model = Author
    list_template = 'library/author_listview.html'
    detail_template = 'library/author_detailview.html'
    per_page = 24
register_crud(blueprint, '/authors', 'authors', AuthorView)


@blueprint.route('/search', methods=['GET', 'POST'])
def search(query=None):
    search_form = SearchForm()

    if request.method == "POST":
        if search_form.validate_on_submit():
            query = request.form['search']
            search_by = int(request.form['search_by'])
            search_results = None

            if search_by == 1:
                search_results = session.query(Book, Author).join(
                    author_books, Author).filter(
                    Book.title.ilike('%' + query + '%')).all()
            else:
                search_results = session.query(Book, Author).join(
                    author_books, Author).filter(
                    Author.name.ilike('%' + query + '%')).all()

            return render_template("library/search_results.html",
                                   query=query,
                                   search_results=search_results,
                                   search_form=search_form)
        else:
            flash_errors(search_form)
    return redirect(url_for('library.home'))


@blueprint.context_processor
def base_contexts():
    search_form = SearchForm(request.form)
    login_form = LoginForm(request.form)
    return {
        'search_form': search_form,
        'login_form': login_form,
    }
