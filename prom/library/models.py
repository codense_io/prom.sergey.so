# -*- coding: utf-8 -*-
"""Library models."""

from prom.database import Table, Integer, Column, ForeignKey, Model, \
    PrimaryPK, String, relationship, backref


# primary_key=True
author_books = Table(
    'library_author_books',
    Column('book_id', Integer, ForeignKey('library_book.id')),
    Column('author_id', Integer, ForeignKey('library_author.id')),
)


class Author(Model, PrimaryPK):
    """A library author model """

    __tablename__ = 'library_author'
    name = Column('Name', String(50), nullable=False)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return u'{name}'.format(name=self.name)


class Book(Model, PrimaryPK):
    """A library book model """

    __tablename__ = 'library_book'
    title = Column('Title', String(80), nullable=False)
    authors = relationship('Author', secondary=author_books,
                           backref=backref('books', lazy='dynamic'))

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return u'{title}'.format(title=self.title)
