# -*- coding: utf-8 -*-
"""Library forms."""
from flask_wtf import Form
from wtforms import StringField, SelectField, SubmitField
from wtforms.validators import DataRequired, Length


class SearchForm(Form):
    """Search form by books or authors"""

    search = StringField(u'search',
                         validators=[DataRequired(), Length(min=3, max=25)])
    search_by = SelectField(u'search by',
                            coerce=int,
                            choices=[
                                (1, 'by book title'),
                                (2, 'by author name')])
    submit = SubmitField(u'search')

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)

    def validate(self):
        """Validate the form."""
        initial_validation = super(SearchForm, self).validate()
        if not initial_validation:
            return False
        return True
