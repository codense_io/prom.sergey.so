# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""

from flask import flash


def flash_errors(form, category='warning'):
    """Flash all errors for a form."""
    for field, errors in form.errors.items():
        for error in errors:
            flash('{0} - {1}'.format(
                   getattr(form, field).label.text, error), category)


def register_crud(blueprint, url, endpoint, AppView, decorators=[], **kwargs):
    model = AppView.model
    view = AppView.as_view(endpoint, endpoint=endpoint, model=model, **kwargs)

    for decorator in decorators:
        view = decorator(view)

    blueprint.add_url_rule('%s/' % url, view_func=view,
                           methods=['GET', 'POST'])
    blueprint.add_url_rule('%s/<int:obj_id>/' % url, view_func=view)
    blueprint.add_url_rule('%s/<operation>/' %
                           url, view_func=view, methods=['GET'])
    blueprint.add_url_rule('%s/<operation>/<int:obj_id>/' %
                           url, view_func=view, methods=['GET'])
