# -*- coding: utf-8 -*-
"""Global app views"""

from flask import flash, redirect, render_template, request, url_for
from flask.views import MethodView
from wtforms.ext.sqlalchemy.orm import model_form
from flask.ext.paginate import Pagination
from flask_login import current_user

from prom.database import session


class ModelView(MethodView):
    """
    Intentionally Django inspired simple parent class for all views.
    Also creates authomatic CRUD forms in templates.
    """

    list_template = None
    detail_template = None
    per_page = 20

    def __init__(self, model, endpoint, list_template=None, context=None,
                 detail_template=None, exclude=None, per_page=None):
        self.model = model
        self.context = context
        self.endpoint = endpoint
        self.path = url_for('.%s' % self.endpoint)
        if list_template:
            self.list_template = list_template
        if detail_template:
            self.detail_template = detail_template
        if per_page:
            self.per_page = per_page
        self.ObjForm = model_form(self.model, session, exclude=exclude)

    def render_detail(self, **kwargs):
        return render_template(self.detail_template,
                               path=self.path,
                               context=self.context, **kwargs)

    def render_list(self, **kwargs):
        pagination = Pagination(total=self.model.query.count(),
                                per_page=self.per_page, bs_version=3)
        return render_template(self.list_template,
                               path=self.path,
                               context=self.context,
                               pagination=pagination, **kwargs)

    def get(self, obj_id='', operation=''):
        if operation == 'new':
            if current_user.is_authenticated:
                form = self.ObjForm()
                action = self.path
                return self.render_detail(form=form,
                                          action=action,
                                          operation=operation)
            else:
                flash('Anonimous user not allow to create this entry!')
                return redirect(self.path)

        if operation == 'delete':
            if current_user.is_authenticated:
                try:
                    obj = self.model.query.get(obj_id)
                except:
                    obj = self.model.query.all()
                session.delete(obj)
                session.commit()
                flash('{} successfull deleted'.format(obj))
                return redirect(self.path)
            else:
                flash('Anonimous user not allow to do this operation!')
                return redirect(self.path)

        if obj_id:
            obj = self.model.query.get(obj_id)
            if current_user.is_authenticated:
                # ObjForm = model_form(self.model, session)
                form = self.ObjForm(obj=obj)
                action = request.path
                return self.render_detail(form=form, action=action)
            else:
                return self.render_detail(item=obj)

        obj = self.model.query.all()
        return self.render_list(obj=obj)

    def post(self, obj_id=''):
        if obj_id:
            obj = self.model.query.get(obj_id)
        else:
            obj = self.model(session)

        # ObjForm = model_form(self.model, session)
        form = self.ObjForm(request.form)
        form.populate_obj(obj)

        session.add(obj)
        session.commit()

        flash('{} successfull saved'.format(obj))
        return redirect(self.path)
