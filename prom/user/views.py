# -*- coding: utf-8 -*-
"""User views."""

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user

from prom.extensions import login_manager
from prom.utils import flash_errors
from prom.user.models import User
from prom.user.forms import LoginForm, RegisterForm


blueprint = Blueprint('user', __name__, url_prefix='/users',
                      static_folder='../static')


@blueprint.route('/', methods=['GET', 'POST'])
def login():
    login_form = LoginForm(request.form)
    if request.method == 'POST':
        if login_form.validate_on_submit():
            login_user(login_form.user)
            flash('You are logged in.', 'success')
            redirect_url = url_for('user.login')
            return redirect(redirect_url)
        else:
            flash_errors(login_form)
    return render_template('library/home.html', login_form=login_form)


@login_manager.user_loader
def load_user(user_id):
    """Load user by ID."""
    return User.get_by_id(int(user_id))


@blueprint.route('/logout/')
@login_required
def logout():
    """Logout."""
    logout_user()
    flash('You are logged out.', 'info')
    return redirect(url_for('library.home'))


@blueprint.route('/register/', methods=['GET', 'POST'])
def register():
    """Register new user."""
    register_form = RegisterForm(request.form, csrf_enabled=False)

    if register_form.validate_on_submit():
        User.create(username=register_form.username.data,
                    email=register_form.email.data,
                    password=register_form.password.data)
        flash('Thank you for registering. You can now log in.', 'success')
        return redirect(url_for('library.home'))
    else:
        flash_errors(register_form)
    return render_template('library/register.html',
                           register_form=register_form)
