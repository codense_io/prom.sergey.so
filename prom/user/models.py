# -*- coding: utf-8 -*-
"""User models."""
import datetime as dt

from flask_login import UserMixin

from prom.database import Column, Model, String, DateTime, PrimaryPK,\
                          reference_col, relationship
from prom.extensions import bcrypt


class Role(PrimaryPK, Model):
    """A role for a user."""

    __tablename__ = 'auth_roles'
    name = Column(String(80), unique=True, nullable=False)
    user_id = reference_col('auth_users', nullable=True)
    user = relationship('User', backref='roles')

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Role({name})>'.format(name=self.name)


class User(UserMixin, PrimaryPK, Model):
    """A user of the app."""

    __tablename__ = 'auth_users'
    username = Column(String(80), unique=True, nullable=False)
    email = Column(String(80), unique=True, nullable=False)
    password = Column(String(128), nullable=True)
    created_at = Column(DateTime, nullable=False, default=dt.datetime.utcnow)
    first_name = Column(String(30), nullable=True)
    last_name = Column(String(30), nullable=True)

    def __init__(self, username, email, password=None):
        self.username = username
        self.email = email
        if password:
            self.set_password(password)
        else:
            self.password = None

    def set_password(self, password):
        """Set password."""
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, value):
        """Check password."""
        return bcrypt.check_password_hash(self.password, value)

    @property
    def full_name(self):
        """Full user name."""
        return '{0} {1}'.format(self.first_name, self.last_name)

    def __repr__(self):
        """Represent instance as a unique string."""
        return '<User({username!r})>'.format(username=self.username)
