$( document ).ready(function() {
  $("select.selectize").selectize();

  $('.item-delete').click(function(e) {
    e.preventDefault();
    datahref=$(this).attr('data-href');
    bootbox.dialog({
      message: "Are you sure?",
      title: "Confirm delete",
      buttons: {
        cancel: {
          label: "Cancel",
        },
        delete: {
          label: "Delete",
          className: "btn-danger",
          callback: function() {
            window.location.replace(datahref);
          }
        }
      }
    });
  });
});